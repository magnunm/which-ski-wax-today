# Expires time map
map $sent_http_content_type $expires {
    default                    off;
    text/html                  epoch;
    text/css                   10d;
    application/javascript     10d;
    application/json           10d;
    ~image/                    max;
}

# NGINX config for static website wax.magnunm.com
server {
  listen 443 ssl http2 default_server;
  listen [::]:443 ssl http2 default_server;

  root /var/www/wax.magnunm.com;

  index index.html;

  server_name wax.magnunm.com;

  location / {
    try_files $uri $uri/ =404;
  }

  expires $expires;

  ssl_certificate /etc/letsencrypt/live/wax.magnunm.com/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/wax.magnunm.com/privkey.pem;

  gzip on;
  gzip_types application/javascript image/* text/css;
  gzip_vary on;
  gzip_comp_level 6;
  gzip_min_length 500;
}

# Redirect http requests to https
server {
  listen 0.0.0.0:80;
  server_name wax.magnunm.com;
  rewrite ^ https://$host$request_uri? permanent;
}
