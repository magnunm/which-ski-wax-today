"""
Simple Lambda backend for wax.magnunm.com.

Handles API requests for weather data. The reason for this
backend is to store the secret API key in a non-public location.
"""
import json
from functools import wraps
import os
from flask import Flask, jsonify, request, Response, make_response
from werkzeug.exceptions import HTTPException
import requests

app = Flask(__name__)

RESPONSE_HEADERS = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
}

WEATHER_API_URL = "https://api.openweathermap.org"


def add_response_headers(wrapped_func):
    """
    Add response headers to the response of a flask view.
    """
    @wraps(wrapped_func)
    def _wrapper__add_response_headers(*args, **kwargs):
        response: Response = wrapped_func(*args, **kwargs)
        response.headers = RESPONSE_HEADERS
        return response
    return _wrapper__add_response_headers


@app.route('/', methods=['GET'])
@add_response_headers
def main():
    args = request.args.to_dict()

    if {'latitude', 'longitude'}.issubset(set(args.keys())):
        return get_weather(args['latitude'], args['longitude'])

    return make_response(
        {'error': 'Missing parameters "latitude", "longitude"'},
        400)


def get_weather(latitude, longitude):
    """
    Call the weather API and return the resulting data wrapped in a flask
    response.
    """
    url_params = {
        'lat': latitude,
        'lon': longitude,
        'appid': os.environ.get("API_KEY")
    }

    weather_response = requests.get(
        f"{WEATHER_API_URL}/data/2.5/weather",
        params=url_params,
        headers={
            'Accept': 'application/json',
        }
    )

    if weather_response.status_code == 200:
        return jsonify(weather_response.json())

    return make_response(
        {'error': 'Something went wrong in fetching weather data'},
        500)


@app.errorhandler(HTTPException)
def handle_http_exception(e):
    """
    Return a JSON 500 response on errors.
    Flask default is a HTML response.
    """
    response = e.get_response()
    response.data = json.dumps(
        {
            'code': e.code,
            'name': e.name,
            'error': e.description
        }
    )
    response.content_type = 'application/json'
    return response
