/* Get weather data based on the current location and use it to
 * decide on the best ski wax and glide.
 *
 * 1. Get longitude and latitude from navigator.geolocation API
 * 2. Pass that information to the lambda backend via a fetch request.
 * 3. Weather data for the closest available location is returned.
 * 5. Decide on the best ski wax.
 */

function main() {
  document.getElementById("en-flag").onclick = () => {
    setLanguage("en");
    translateDocument();
  };
  document.getElementById("no-flag").onclick = () => {
    setLanguage("no");
    translateDocument();
  };

  const suggestionElement = document.getElementById("wax-suggestion");
  const suggestionBasisElement = document.getElementById("suggestion-basis");
  const allowLocationElement = document.getElementById("allow-location");

  suggestionElement.stringId = "empty";
  suggestionBasisElement.stringId = "empty";

  function onWeatherDataSuccess(temperature, position) {
    writeWaxSuggestion("wax-suggestion", "wax-suggestion-img", temperature);

    allowLocationElement.stringId = "empty";

    suggestionBasisElement.stringId = "suggestion-basis";
    suggestionBasisElement.templateStringParameters = [
      temperature.toFixed(1),
      position
    ];

    // Update the document with the new information
    translateDocument();
  }

  function onWeatherDataError(error) {
    console.warn(JSON.stringify(error));
    allowLocationElement.stringId = "weather-data-error";
    translateDocument();
  }

  function onGeolocationError(error) {
    console.warn(`ERROR(${error.code}): ${error.message}`);

    if (error.code === 1) {  // Blocked position request
      allowLocationElement.stringId = "allow-location";
    }
    else {
      allowLocationElement.stringId = "geolocation-error";
    }
    translateDocument();
  }

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        getWeather(
          position,
          onWeatherDataSuccess,
          onWeatherDataError
        );
      },
      onGeolocationError
    );
  } else {
    allowLocationElement.stringId = "geolocation-browser-support";
    translateDocument();
  }
}

/**
 * Get temperature and nearest weather location for a given position.
 */
async function getWeather(position, onSuccess, onError) {
  // URL of lambda backend
  const urlBase = "https://msc7io6e87.execute-api.eu-central-1.amazonaws.com/production";
  const url = `${urlBase}?latitude=${position.coords.latitude}&longitude=${position.coords.longitude}`;

  const response = await fetch(url, {});
  const responseJson = await response.json();

  if (!response.ok) {
    onError(responseJson);
    return;
  }

  // Kelvin to degrees Celsius
  const temperature = responseJson.main.temp - 273.15;
  const location = `${responseJson.name} ${responseJson.sys.country}`;

  onSuccess(temperature, location);
}

/**
 * Given a temperature, write the corresponding wax suggestion to the document.
 */
async function writeWaxSuggestion(elementId, imgElementId, temperature) {
  let wax, waxProperties;
  [wax, waxProperties] = await waxSuggestion(temperature);

  const element = document.getElementById(elementId);
  element.stringId = wax;

  if (waxProperties.img) {
    const imgElement = document.getElementById(imgElementId);
    imgElement.src = waxProperties.img;
  }

  translateDocument();
}

/**
 * The suggested wax and its properties, given the temperature.
 */
async function waxSuggestion(temperature) {
  // TODO: Add snow condition dependence
  const waxesText = await fetch("wax.json").then((r) => r.text());
  const waxes = JSON.parse(waxesText);

  const {defaultWax, ...waxesExceptDefault} = waxes;

  const waxMatchingTemperature = Object.entries(waxesExceptDefault).find(
    ([_, value]) => value.low <= temperature && temperature < value.high
   );

  if (!waxMatchingTemperature) {
    return ["defaultWax", defaultWax];
  }
  return waxMatchingTemperature;
}

/**
 * Update all strings with translations in the document.
 */
function translateDocument() {
  getString("title", (string) => {
    document.title = string;
  });

  // We use the class `translated` to mark elements that need translation.
  document.body.querySelectorAll(".translated").forEach((node) => {
    translateNode(node);
  });
}

function translateNode(node) {
  function setNodeString(string, isTemplate) {
    if (isTemplate) {
      node.innerHTML = evaluateTemplateString(
        string,
        node.templateStringParameters
      );
    } else {
      node.innerHTML = string;
    }
  }

  let stringId = node.stringId;
  if (stringId === undefined) {
    stringId = node.id;
  }

  if (stringId) {
    getString(stringId, setNodeString);
  }
}

/**
 * Get a string by string id and pass it to a callback string handler.
 *
 * The second argument to the string handler signals wether or the
 * passed string is a template. It is up to the string handler to then
 * correctly handle the template string.
 */
async function getString(stringId, stringHandler) {
  if (stringId === "empty") {
    // It is important that the string id representing the empty string is
    // not itself the empty string. This is becuse a element without an
    // `id` acually has the `id` property equal to the empty string. Therefore
    // we have the keyword "empty" for the empty string here.
    stringHandler("", false);
    return;
  }

  const language = getLanguage();

  const stringsFile = await fetch("./strings.json");
  const stringsText = await stringsFile.text();
  const string = JSON.parse(stringsText)[stringId];

  if (!string) {
    throw Error(`The string id ${stringId} was passed, but it does not exist in strings.json`);
  }

  stringHandler(string[language], string.template);
}

/**
 * Evaluate the given template string `string` using the `templateParameters`.
 *
 * Template syntax: substrings consisting of a hash (#) followed by a digit
 * are replaced by the element at the corresponding index in the template
 * parameters. To prevent a hash followed by a digit from being replaced prepend
 * it with two backslashes.
 */
function evaluateTemplateString(string, templateParameters) {
  if (!templateParameters) {
    throw Error("No template parameters passed");
  }

  // Replace a substring matching the template pattern. The first group is
  // the negated escape sequence double backslash (\\) and the second group is
  // template pattern (hash + digit).
  function replaceTemplateMatch(match, group1, group2) {
    const index = parseInt(group2.slice(1));

    if (templateParameters[index]) {
      return group1 + String(templateParameters[index]);
    }

    throw Error(`Template string contained template '${match}' but there was no element with index ${index} in the passed template parameters.`);
  }

  return string.replaceAll(
    // hash + digit not prepended by two backslashes
    /([^\\\\])(#\d+)/g,
    replaceTemplateMatch
  ).replaceAll(
    // hash + digit prepended by two backslashes
    /([\\\\])(#\d+)/g,
    "$2"
  );
}

function setLanguage(language) {
  localStorage.setItem('language', language);
}

function getLanguage() {
  let language = localStorage.getItem('language');
  if (!language) {
    language = "en";
  }

  return language;
}

main();
