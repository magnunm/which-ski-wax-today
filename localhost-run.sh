#!/bin/bash
# Serve the site on localhost with nginx. Watch for file changes and update
# accordingly. Requires inotify-tools.

if [[ -z $(which inotifywait) ]]; then
    printf "This sqript requires inotify-tools"
    exit 1
fi

deploy_and_run_localhost() {
    sudo cp -r -t /var/www/localhost index.* *.json assets
    sudo cp nginx/localhost /etc/nginx/sites-available
    sudo ln -sf /etc/nginx/sites-available/localhost /etc/nginx/sites-enabled/localhost

    sudo nginx -qt && sudo systemctl restart nginx
}

watch_for_file_changes() {
    # Get the directory of this shell script
    dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

    # Watch for modification in the directory.
    # Excluding files starting with .# which are modfified Emacs buffers.
    # This means we wait only for writes (as long as the editor used is Emacs).
    inotifywait -mrq -e modify --exclude "^\.#.{1,}" $dir |
        while read dir event file; do
            deploy_and_run_localhost
        done
}

deploy_and_run_localhost

watch_for_file_changes &

printf "Watching for file changes at pid $! ✅\n"
printf "Running on http://localhost ✅\n"

while [[ true ]]; do
    read -n 1 -p "Type q to kill: " action
    printf "\n"

    if [[ "$action" = "q" ]]; then
        # Stop the webserver
        sudo rm /etc/nginx/sites-enabled/localhost
        sudo nginx -qt && sudo systemctl restart nginx

        # Stop the watcher by killing the current process group id.
        # Negative sign denotes this is a GPID not a PID, the double dash
        # (--) is then needed to signal that this is not a option.
        kill -- -$$
        exit 0
    fi
done
